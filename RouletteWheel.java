import java.util.Random;
public class RouletteWheel{
    private Random rand;
    private int lastSpin;
    final int MAX_NUMBER = 38;

    public RouletteWheel() {
        rand = new Random();
        lastSpin = 0;
    }
    public void spin() {
        //genereating numbers from 0-36
        int randomNumber = rand.nextInt(MAX_NUMBER);
        this.lastSpin = randomNumber;
    }
    public int getValue(){
        return lastSpin;
    }
//public static void main(String[] args) {
// RouletteWheel test = new RouletteWheel();
// test.spin();
//  System.out.print(test.getValue());
//}
    
}